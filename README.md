# ImageAI Object Detection API 
## Setup

`docker run -p 80:80 registry.gitlab.com/sleepless-se/imageai_object_detection_api:latest`

## Test
**Access** 
http://localhost

You can see `Welcome to Object detection!`

**Object ditection**

Send your image to the API. It's return the result of object detection by YOLOv3. 

`curl -X POST -F image=@path/to/image.jpg http://localhost/predict`

Please replace to your image path `path/to/image.jpg`

## Develop

You can edit ImageAI API. 

1. Clone repository `git clone git@gitlab.com:sleepless-se/imageai_object_detection_api.git`
1. Edit `imageai_object_detection_api/api.py` as you want.

Here is [ImageAI Document](https://imageai.readthedocs.io/en/latest/)

## Build Push

If you are fork on GitLab it's automatically build and push on the GitLab Container Registry. 
