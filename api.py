#!/usr/bin/env python
# coding: utf-8

# # Object Detection Demo
# Welcome to the object detection inference walkthrough!  This notebook will walk you step by step through the process of using a pre-trained model to detect objects in an image. Make sure to follow the [installation instructions](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/installation.md) before you start.

# # Imports

# In[12]:


import numpy as np
import os
import tensorflow as tf
from PIL import Image
import time
from flask import Flask
from flask import request
from flask import jsonify
import json
from imageai.Detection import ObjectDetection

EXCUTION_PATH = '/app'

def load_model():
    yolo = ObjectDetection()
    yolo.setModelTypeAsYOLOv3()
    yolo.setModelPath(os.path.join(EXCUTION_PATH, "yolo.h5"))
    yolo.loadModel()
    return yolo

model = load_model()
graph = tf.get_default_graph()

class MyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        else:
            return super(MyEncoder, self).default(obj)

def image_to_numpy_array(image):
  (im_width, im_height) = image.size
  return np.array(image.getdata()).reshape(
      (im_height, im_width, 3)).astype(np.uint8)


def predict_image(image:Image):
    image = Image.open(image)
    image = image.convert("RGB")
    image = image_to_numpy_array(image)
    print("start timer")

    with graph.as_default():
        output_image = os.path.join(EXCUTION_PATH , "last_image.jpg")
        start = time.time()
        detections = model.detectObjectsFromImage(input_image = image ,input_type = 'array', output_image_path = output_image)
        print('sec',time.time()-start)
        return detections
    return None

app = Flask(__name__)


@app.route("/")
def home():
    return "Welcome to Object Detection API!"

@app.route("/predict", methods=['POST'])
def predict():
    if request.method == 'POST':
        data = request.files
        try:
            data = predict_image(data['image'])
            print(data)
            data = json.dumps(data, cls=MyEncoder)
            return jsonify(data)
        except Exception as e:
            print(e)
            return "ERROR:{}".format(e)
    else:
        return "no post"



if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5000, debug=True)

